all: build;

build:
	killall latexmk; killall xelatex; latexmk -xelatex -synctex=1 index.tex

stop:
	killall latexmk; killall xelatex;

relise: build clean
	pdfunite tit.pdf main.pdf 4202\ Бадрутдинов\ КР.pdf
	
run:
	okular main.pdf &
	
clean:
	rm *.aux \
	*.fdb_latexmk \
	*.fls \
	*.log \
	*.out \
	*.xdv \
	*.synctex.gz \
	*.toc
